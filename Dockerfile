FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /project
ADD ./app /project
RUN pip install -r requirements.txt